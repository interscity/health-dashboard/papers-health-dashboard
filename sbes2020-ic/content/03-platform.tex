\section{The visualization platform}
\label{sec:platform}

Our platform focuses on promoting better analysis for georeferenced hospitalization data through a data visualization dashboard. The implemented architecture aims to deal with any SIH-SUS dataset, making possible the analysis of public hospitalizations from any region of Brazil. 

A useful data visualization dashboard should present essential information and concisely summarize all the data that matters, as well as must be interactive, allowing an easy data exploration. The platform should also enable data import and export in different formats, such as PDF and CSV. Besides, for the proper analysis of georeferenced datasets, the dashboard should provide features for the spatial visualization of data. Our platform meets all these requirements while supporting the analysis of the SIH-SUS large dataset.

We defined design principles to build a feasible, robust, and concrete dashboard application architecture. These principles contribute to the software extensibility and maintainability. 
The platform needs to contain \texttt{modularity}, at some level. It facilitates the comprehension of the code and increases its cohesion due to its division in logical components. Modularity also improves communication between parts of the code. Moreover, it enables the full test coverage because it is easier to test small modules than the entire code base. The \texttt{reuse of free software projects} can improve faster code development, save costs, and enhance code reliability. However, it is essential to be aware of the package quality and to use well known free projects with an active developer community. We also advocate the use of  \texttt{\textit{Don't Repeat Yourself} principle} (DRY). The idea behind DRY is the need for every piece of knowledge in the development of something to have a single representation \cite{David:2000}. Furthermore, a piece of knowledge can be either the build system, the database schema, the tests, or even the documentation.

%We needed to define these principles because the previously planned architecture based on \textit{Microservices} resulted in unsatisfactory performance and a non-generalizable system, that has not facilitated the use of SIH-SUS datasets from other Brazil regions. We rewrote our implementation, also evaluating that the best maintenance way to face the project issues, adopting the MVC (Model-View-Control) architectural style\cite{Krasner:1988}. The new architecture brought performance benefits over our microservices-based strategy and enabled the possibility to use other SIH-SUS datasets.

The definition of these principles resulted from our efforts to improve the performance and generalizability of the platform. We rewrote our implementation adopting the MVC (\textit{Model-View-Control}) architectural style\cite{Krasner:1988}, this time also considering system maintainability and extensibility as primary concerns. The new architecture brought performance benefits over the first version of the platform and better enabled it to receive other SIH-SUS datasets over our microservices-based propose.

\subsection{Microservices-based strategy}
\label{sec:previous_solution}

The first version of the platform had a monolithic architecture, \textit{i.e.}, it was a software application containing interconnected components designed without modularity. During the development of this oldest version, the number of added features was increasing, as well as its codebase size. However, this brought to the platform some problems like:
\begin{itemize}
  \item A significant complexity directly caused by the large codebase;
  \item High difficulty and time consuming for a developer to understand the code;
  \item Big challenge to adopt new technologies and programming languages;
  \item Difficulty to fully test the entire system due to its complexity, and greater risk of releasing bugs to production;
  \item Disruption of the whole system in case of a failure.
\end{itemize}

We decided to migrate to a microservices-based approach to solve the problems mentioned above. Furthermore, a service‑oriented architecture could also facilitate creating different applications for visualization of spatial data. Thus, we proposed and designed an architecture based on three principal component layers: applications, service\footnote{\url{https://gitlab.com/interscity/health-dashboard/datahealth-api}}, and databases, providing communication via REST (Representational State Transfer) calls. We sought a microservice-based architecture to abstract the data manipulation layers to offer applications an easy-to-use API for inserting and querying data regardless of its source.

However, during its implementation, we have analyzed the difficulties from this approach, particularly regarding its maintenance in the future for our partner. Therefore, we investigated the factors that must be considered when evaluating the suitability of the service‑oriented architecture\cite{Taibi:2017}. The next sections discuss the advantages and disadvantages of using microservices in our case.

\subsubsection{Advantages of microservices}
\label{subsec:advantages_microservices}

Moving to a microservices-based approach makes application development faster and easier to manage \cite{Richardson:2016}. It happens because of its modularity. The independence of its services promotes many features to a platform \cite{Richardson:2019}.

The architecture modularity decomposes the application into a set of services. It contributes to the understanding of the code and also combats the complexity problem. This migration also promotes deployment and scalability independence. Each service can be independently deployed on the hardware that satisfy its requirements. This approach would make continuous deployment possible.

The independence among the system's components promotes the autonomy of development teams, delegates placed responsibilities, removes the need for synchronization between them, and fosters parallelization. The developers are free to choose the technology to be employed for a new service. It also facilitates the refactoring of an existing service to a new technology or language. Moreover, it enables different components to use different programming languages and technologies.

Finally, its relatively smaller services improve the test coverage because of the codebase's division into modules (that are easier to cover).
Moreover, the failure of one component does not disrupt the whole platform.

\subsubsection{Disadvantages of microservices}
\label{subsec:advantages_microservices}

Microservices are not silver bullets, like every other technology \cite{Brooks:1975}. Thus, there are also drawbacks and issues, and re-architecting a platform must be based on real facts and actual issues from the current application.

The migration to microservices results in some aspects to the future system, but not always our needs or available resources meet these points. First, the decomposition of the application into different services aims to facilitate the understanding of one service for a team, because each team usually handles few services or only one. If there is just one team for the entire application's maintenance, this division could increase the difficulty to understand the system and contribute to its evolution.
Moreover, each component could use different technologies, and it becomes arduous to only one team deal with this kind of complexity.
The platform we are developing is intended to be used and maintained by public entities. They could not be able to sustain multiple teams to manage the platform like companies usually do.

Additionally, microservices were proposed to solve industry problems, and they may not be computational viable for our application.
Each service of the architecture carries its specific deployment, resource, scaling, and monitoring requirements \cite{Richardson:2016}.
In this context, the limited available resources at public entities might be an infrastructure bottleneck and impact important topics like deployment and scalability.

\subsection{MVC strategy}
\label{sec:final_architecture}

We implemented an architecture for a platform that enables the visualization of any SIH-SUS dataset. We chose the software architectural style \texttt{Model-View-Controller} (MVC)~\cite{Krasner:1988} as the core of the platform. 
\texttt{Model} components are those parts of the system application that simulate the application domain. In our case, the models represent the SIH-SUS dataset inside the platform. \texttt{Views} deal with everything graphical. They display aspects of their models. As mentioned before, the hospitalization \textit{Procedure} model is the main model in the platform. \texttt{Controllers} contain the interface between their associated models and views and the input devices. They send messages to the model and provide the interface between the model and its related views  \cite{Krasner:1988}. The MVC style provides flexibility that decreases the code complexity and brings modularity to the system \cite{GOF:1995}. Thus, the platform obtains, on some level, benefits such as code reuse, high cohesion (due to MVC logical grouping), and joint development (because of the code modularity and independence).

\begin{figure}[ht]
  \centering
    \includegraphics[width=.7\textwidth]{figures/architecture}
  \caption{Basic software components of the platform}
  \label{fig:components}
\end{figure}

Figure \ref{fig:components} shows the software components of the developed platform. The \texttt{Webserver} is the component where we implemented the MVC, using the Ruby on Rails framework, with the domain logic that encodes the business rules. It extensively uses the other two components to perform searches and display results: \texttt{PostgreSQL} stores the data while \texttt{Redis} is used as a cache server.

The SIH-SUS dataset structure may subtly differ from region to region, so we also need a \texttt{database generalizer} to receive the different SIH-SUS datasets and make their visualization possible despite these differences. The importation of a SIH-SUS dataset characterizes this generalization into the platform. A dataset is imported in the platform through a CSV file containing some predefined columns (such as the hospitalization admission date, the health facility code, the patient age, and 26 other fields). 

%Despite not containing the patient name or his exact residence location (it contains only his census tract location), it can be possible to track rare disease cases. 
The platform does not store patient identifiable information such as name and exact residence location, it only stores his census tract. However, it can be possible to identify a patient who has a rare disease through his census tract. For example, a census tract can be a single building and, within this building, there may be a single case of a given disease.
%To overcome this exposure, the dataset is encrypted and stored within the project throughout the deployment process without exposing any data to unauthorized individuals. 
To overcome this exposure, for controlling data access in the platform, we use the \texttt{Devise} Gem of Ruby on Rails for user authentication. It uses the \textit{Bcrypt} password hashing algorithm and saves encrypted passwords directly on the database. This way, the platform securely stores data avoiding unauthorized accesses.

In terms of our SIH-SUS dataset, we are dealing with more than half a million records dataset, and such a large number of records can also become an obstacle to data analysis, from both visualization and response time perspectives. Classifying or grouping the data into a set of categories is a way to solve the visualization problem. Our platform provides a spatial visualization through clusters since the data is displayed on a geographic map. With the computational clustering approach, the platform supports public health professionals in performing analyses and understanding geographic patterns. The platform addresses the response time issue by employing denormalization techniques \cite{Sanders:2001} and by caching to \texttt{Redis} the generated \texttt{JavaScript} code for each individual data point. %In short, through a simple implemented architecture, we developed essential features to the platform.


\subsection{Features}
\label{sec:features}

From the SIH-SUS dataset of our partner in the platform, the user can view a map with information about hospitalizations and health facilities. It is possible to import a SIH-SUS dataset from any other region since it fits the database generalizer script requirements.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{figures/healthdashboard}
  \caption{Example of an Advanced Search displaying all hospitalizations due to a specific disease}
  \label{fig:advanced_search}
\end{figure}

The developed platform is a web application divided into two different pages for different purposes. The main page is called \texttt{Advanced Search} and is presented in Figure \ref{fig:advanced_search}. It contains the view of the \textit{Procedure} model. It also includes the patients' geo-anonymized locations, the locations of the health facilities they were admitted, and the region's hospitalization concentration. Inside the Advanced Search page, we can find the right side menu with the filters -- Health facility (\textit{Estabelecimento}), Procedure (\textit{Procedimento}), Patient Data (\textit{Informações do Paciente}), and Heatmap (\textit{Mapa de calor}) --, and the options to control the map visualization. 

The three first filter sections refer to the data itself while the fourth selects the total population used to compute the heatmap weighted by each census tract population, enabling to restrict it by gender and race. The data referring to filter fields from the first three sections enables a database search interface. It allows the visualization of the hospitalization information on the map by choosing any attribute of the dataset as a filter. The first section, Health facility, contains fields such as the health facility name, administration, and beds. The second, the hospitalization data (also called Procedure) section, includes the patient diagnosis and hospitalization date. The third section, Patient Data, includes the patient's age, gender, and scholarly level. We can search by these composite filters and receive the hospitalization data that satisfies the query. This engine complies with the need to access the information in a large dataset. Through the selected filters, the platform supports a space-time pattern identification, \textit{i.e}. it enables the analysis of geographic and temporal variations of the hospitalizations over the city.

The data is visualized in a map through a heatmap and clusters representations. The colorful heatmap represents the intensity of the hospitalizations in each region. The clusters represent the quantity of these hospitalizations. Data is grouped by distance-based clustering over the location of the health facility where the hospitalization occurred.  An orange cluster contains more than 100 group records, a yellow cluster contains less than 100, and a green cluster contains all the hospitalization records of a given census tract.

The platform also contains a page called \texttt{General Data}, composed of a large variety of charts. Each attribute of the database can be displayed in a chart on this page. We designed all these features to meet functional requirements identified in meetings with our partner, aiming to provide a proper tool for public health management purposes.

Within our platform, the user can evaluate specific areas' health situation, the hospitalizations distribution on the map over time, area, diagnosis, and over many other variables. The user can also be aware of the distance traveled \footnote{Using the Open Source Routing Machine (OSRM) software, we calculated the distance by available public roads to provide a better estimation of how much a patient traveled to get medical assistance.} by patients to be hospitalized. The use of the Advanced Search page to analyze the distribution of hospitalizations by a specific health facility is another example. The user can retrieve this information over different periods and analyze the evolution of a specific scenario.

