\section{Development Approach}

Our platform focuses on promoting better analysis for georeferenced hospitalization data through a data visualization dashboard. Its implementation aims to deal with any SIH-SUS dataset, making possible the analysis of public hospitalizations from any region of Brazil.

A useful data visualization dashboard should present essential information,  concisely summarize all the data that matters, and be interactive, allowing easy data exploration. The platform should also enable data import and export in different formats, such as PDF and CSV. Besides, for the proper analysis of georeferenced datasets, the dashboard should provide features for the spatial visualization of data. Our platform meets all these requirements while supporting the analysis of the SIH-SUS large dataset.

We developed the platform in a government-academia collaboration. This scenario is often challenging, and both parties need to strive to increase the chances of success. Interinstitutional conflicts should be mitigated to obtain a successful collaboration~\cite{Wen:2019}. In our collaboration with a Brazilian Health Secretariat, we adopted practices from agile methods and Open Source Software (OSS) communities, following the lessons learned by Wen et al.~\cite{Wen:2019} to involve the Brazilian public health officials (expert users) in the development process. They participated, updated, and created issues (features and bugs) into the project repository based on the requirements defined by them. The government officials and expert users interacted directly with the development team via the project's repository and received on-the-fly notifications of advances and difficulties we were facing while receiving frequent feedback from their homologation tests. We also had monthly strategy meetings with them, which helped us identify the best technical approach to fit their needs and the available resources.

Development activities occurred based on project maintainers and developers' contributions. Project maintainers can update the main source code, while project developers can update the code through merge requests (MR). In this way, we adopt a development workflow based on OSS communities, intending to provide the source code quality through submitted code inspections~\cite{scacchi2006}. In summary, each contributor can make a copy (fork) of the project repository, change the source code, and return these changes to the principal repository via merge requests. From that point, a maintainer should review the submitted code and, eventually, request modifications and improvements. Finally, when the submitted code is within the project's quality standards, a maintainer can accept the MR and incorporate the code changes. Our project's code and issues are available at a GitLab repository \footnote{\url{https://gitlab.com/interscity/health-dashboard}} under the Mozilla Public License 2.0.


\section{Architecture}
\label{sec:previous_solution}

We adhered to well-established design principles to build a feasible, robust, and concrete dashboard application architecture. These principles contribute to the software extensibility and maintainability. The platform requires \texttt{modularity} since it facilitates the code's comprehension and increases its cohesion due to the division in logical components~\cite{Sullivan2001}. Modularity also improves communication between parts of the code and facilitates full test coverage because it is easier to test small modules than the entire code base. The \texttt{reuse of OSS projects} can improve faster code development, save costs, and enhance code reliability~\cite{scacchi2006}. However, it is essential to be aware of the package quality and to use well known free projects with an active developer community. We also follow the \texttt{\textit{Don't Repeat Yourself} principle} (DRY) in our development. It states that every piece of knowledge needs to have a single representation~\cite{David:2000}. Furthermore, a piece of knowledge can be either the build system, the database schema, the tests, or even the documentation.

\begin{figure}[ht]
  \centering
    \includegraphics[width=\linewidth]{figures/architectureV2}
  \caption{Basic software components of the platform}
  \label{fig:components}
\end{figure}

\begin{figure*}[ht]
  \centering
  \includegraphics[width=.95\textwidth]{figures/healthdashboard_new}
  \caption{An Advanced Search displaying all hospitalizations due to a specific disease}
  \label{fig:advanced_search}
\end{figure*}

Based on these principles, we adopted the MVC (\textit{Model-View-Controller}) architectural style\cite{Krasner:1988} to implement our platform, considering system maintainability and extensibility as fundamental concerns. \texttt{Model} components are those parts of the system application that simulate the application domain. In our case, models represent the SIH-SUS dataset inside the platform. The hospitalization \textit{Procedure} model is the main model in the platform. \texttt{Views} deal with everything graphical; they display aspects of their models. \texttt{Controllers} contain the interface between their associated models and views and the input devices. They send messages to the model and provide the interface between the model and its related views  \cite{Krasner:1988}. The MVC style provides flexibility that decreases code complexity and brings modularity to the system \cite{GOF:1995}. Thus, the platform obtains, on some level, benefits such as code reuse, high cohesion (due to MVC logical grouping), and joint development (because of the code modularity and independence).

Figure \ref{fig:components} shows the software components of the developed platform. The \texttt{Webserver} is the component where we implemented the MVC, using the Ruby on Rails framework, with the domain logic that encodes the business rules. By itself, Rails does not provide storage for the model's data, requiring connection to an external relational \texttt{database}. With a large dataset, queries to the database can be a performance-intensive task, causing responsiveness issues on the platform. One solution to this issue is using a \texttt{cache server}, where the answers for the most frequent queries can be stored to speed up data access. This solution avoids wasting time on work that has already been done, increasing the system's overall performance and responsiveness. Currently, our platform uses \textit{PostgreSQL} as a database management system and \textit{Redis} as a cache server.

The SIH-SUS dataset structure may subtly differ from region to region, so we also need a \texttt{database generalizer} to receive the different SIH-SUS datasets and make their visualization possible despite these differences. The importation of a SIH-SUS dataset characterizes this generalization into the platform. A dataset is imported into the platform through a CSV file containing some predefined columns (such as the hospitalization admission date, the health facility code, the patient age, and 26 other fields). 

The platform does not store identifiable patient information such as name and exact residence location; it only stores his census tract. However, it can be possible to identify a patient with a rare disease through his census tract. For example, a census tract can be a single building, and, within this building, there may be a single case of a given disease. To overcome this exposure, we use the \texttt{Devise} Gem of Ruby on Rails for user authentication to control data access in the platform. It uses the \textit{Bcrypt} password hashing algorithm and saves encrypted passwords directly on the database. This way, the platform securely stores data avoiding unauthorized access.

In terms of our SIH-SUS dataset, we are dealing with more than half a million records, and such a large number can also become an obstacle to data analysis from both visualization and response time perspectives. Classifying or grouping the data into a set of categories is a way to solve the visualization problem. Our platform provides a spatial visualization through clusters since the data are displayed on a geographic map. With the computational clustering approach, the platform supports public health professionals in analyzing and understanding geographic patterns. The platform addresses the response time issue by employing denormalization techniques \cite{Sanders:2001} and caching to \texttt{Redis} the generated \texttt{JavaScript} code for each data point.

\section{Features}
\label{sec:features}

The user can view a map with information about hospitalizations and health facilities from the SIH-SUS dataset of our partner in the platform. It is possible to import a SIH-SUS dataset from any other region since it fits the database generalizer script requirements.

The platform is a web application divided into two different pages for different purposes. The main page is called \texttt{Advanced Search} and is presented in Figure \ref{fig:advanced_search}. It contains the \textit{Procedure} model's view, featuring the patients' geo-anonymized locations, the health facilities' locations, and the region's hospitalization concentration. Inside the Advanced Search page, we can find the right side menu with the filters -- Health facility, Procedure, Patient Data, and Heatmap by hospitalizations rate -- to control the map visualization and the export buttons. 

\begin{figure}[ht]
  \centering
  \includegraphics[width=.95\linewidth]{figures/healthdashboard_facility}
  \caption{An Advanced Search displaying all hospitalizations from two specific health facilities with their respective percentile circles}
  \label{fig:advanced_search_facility}
\end{figure}

The three first filter sections refer to the data itself, while the fourth selects the total population used to compute the heatmap weighted by each census tract population, enabling to restrict it by gender and race. The data referring to filter fields from the first three sections enables a database query interface. It allows the visualization of the hospitalization information on the map by choosing any database attribute as a filter. The first section, Health facility, contains fields such as the health facility name, administration, and beds. The second, the hospitalization data (also called Procedure) section, includes the patient diagnosis and hospitalization date. The third section, Patient data, includes the patient's age, gender, and educational level. We can search by these composite filters and receive the hospitalization data that satisfies the query. This engine complies with the need to access the information in a large dataset. The platform supports a space-time pattern identification through the selected filters, \textit{i.e.}, it enables the analysis of geographic and temporal variations of the hospitalizations over the city.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\linewidth]{figures/general_data}
  \caption{Database info and applied filters on \texttt{General Data} page}
  \label{fig:general_data}
\end{figure}

The data are visualized on a map through a heatmap and clusters representations. The colorful heatmap represents the intensity of the hospitalizations in each region. The clusters represent the quantity of these hospitalizations. The data are grouped by distance-based clustering over the location of the health facility where the hospitalization occurred.  An orange cluster contains more than 100 group records, a yellow cluster contains less than 100, and a green cluster contains all the hospitalization records of a given census tract. The map also supports visualization of administrative limits, \textit{e.g.}, city limits and sub-prefectures, and health facilities percentiles circles.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\linewidth]{figures/ranking}
  \caption{Ranking of health facilities (anonymized here) with more hospitalizations due to a specific disease}
  \label{fig:ranking}
\end{figure}

Figure \ref{fig:advanced_search_facility} displays another example from the Advanced Search page. It depicts hospitalizations from two different health facilities: a highly-specialized hospital from the University of S\~ao Paulo that provides high-complexity services and a local, small hospital. The concentric circles show the areas in which the patients of each of these hospitals reside. The blue, green, and red circles show the region in which 25, 50, and 75\% of the patients reside. This functionality allows for the analysis of the geographical coverage of different health facilities. In this example, the university hospital serves the entire city, and the other one serves only close neighborhoods. Have in mind that we are dealing with patients coming majorly from middle- and low-income strata, and they live in a large metropolis where transportation can be a real problem. Thus, analyzing the location of the facilities and the residence of their patients is a very relevant issue.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\linewidth]{figures/information}
  \caption{Hospitalization section with information of patients and hospitalizations due to a specific diagnosis}
  \label{fig:information}
\end{figure}

The platform also contains a page called \texttt{General Data}, composed of a wide variety of charts. Each database attribute is displayed in a chart on this page, subject to the \texttt{Advanced Search} page's selected filters. We designed all these features to meet functional requirements identified in meetings with the healthcare experts from our partner, aiming to provide a proper public health management tool.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\linewidth]{figures/distance}
  \caption{Partial view of the Distances section}
  \label{fig:distance}
\end{figure}

The \texttt{General Data} page's top shows the database info and all the current applied filters (Figure \ref{fig:general_data}). The Ranking section contains tables ordered by the number of hospitalizations in different data attributes. This feature allows the observation of which health facilities have more hospitalizations, as illustrated in Figure \ref{fig:ranking}.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\linewidth]{figures/choropleth_map}
  \caption{Number of hospitalizations per district}
  \label{fig:choropleth_map}
\end{figure}


The Health Facilities section presents charts informing the number of hospitalizations by health facility type and the number of beds in each health facility. The Hospitalization section  (Figure \ref{fig:information}) shows hospitalizations by the number of days in the hospital. It is also available in this section the proportion of hospitalization types and patient gender, age, and race information.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\linewidth]{figures/statistics}
  \caption{Partial view of the Descriptive Statistics section}
  \label{fig:statistics}
\end{figure}


Figure \ref{fig:distance} shows the Distances section, containing data related to the distances between patients' homes and health facilities where they are serviced \footnote{Using the Open Source Routing Machine (OSRM) software, we calculated the distance via city streets to better estimate how much a patient traveled to get medical assistance.}. The mean distance traveled for each specialty can be found in this section. There is also another distance-related section with a chart for each health facility presenting the proportion of hospitalizations in four distance groups for each one of the specialties. Considering that a low-income patient may spend up to 1 or 2 hours in a crowded bus to reach the proper health facility, this is a significant concern.

Hospitalization data in four different administrative levels are displayed on choropleth maps in the Territories section. Figure \ref{fig:choropleth_map} shows one of these maps. The Dynamic Chart section produces individual charts based on a list of relevant categories given by our partner, including treemaps with the number of hospitalizations by diagnosis in the ICD-10.

As shown in Figure \ref{fig:statistics}, the Descriptive Statistics section contains a table with the sum, the minimum, the maximum, the mean, and the standard deviation of some database attributes like patient age, days in the hospital, or ICU days. Finally, the Census Tract section shows a table with patient data distribution by gender and race for each census tract (the smallest area division in the platform).

Both pages support exports of the view. In the \texttt{Advanced Search} page, users can download the resulting data as a CSV file, a filtered subset of the original dataset, or a PDF file containing the map view, applied filters, legend, and source. In the \texttt{General Data} page, users can export each section as a PDF file, including all the selected section charts and their source.

Within our platform, healthcare expert users can evaluate the health situation in specific regions, the hospitalizations distribution on the map over time, area, diagnosis, and many other variables, according to the requirements and homologation tests from the Brazilian public health officials of our partner. The users can also be aware of the distance traveled by patients to be hospitalized. The use of the Advanced Search page to analyze the distribution of hospitalizations by a specific health facility is another example. An expert user can retrieve this information over different periods and analyze the evolution of a specific scenario.





